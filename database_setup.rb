require 'database.rb'

# create an items table
DB.create_table :behance_links do
  primary_key :id
  String :name
  String :link
  Float :completed
end