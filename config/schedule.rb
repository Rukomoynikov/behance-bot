set :output, "/var/www/behance-bot/cron_log.log"

every 10.minute do
  command "cd /var/www/behance-bot/current && DISPLAY=:1 rake"
end

every 3.minute do
  command "cd /var/www/behance-bot/current && DISPLAY=:1 rake add_comments"
end

every 1.day, :at => '23:50 am' do
  command "cd /var/www/behance-bot/current && DISPLAY=:1 rake save_stats && rake reset_account_counter"
end