require 'rest-client'
require 'json'
require_relative './database.rb'
require 'net/http'

class BotAPI
  attr_accessor :client_key, :user

  def initialize
    @client_key = 'RmvCiLg6mbMkKX6cyWmud44bYIC1jvLy'
    @user = 'apus'
    @statsDB = DB[:behance_stats]
  end

  def generateUserInfo
    stats = getData("https://api.behance.net/v2/users/#{user}/stats?client_id=#{@client_key}")
    followers = getData("https://api.behance.net/v2/users/#{user}?client_id=#{@client_key}")

    userInfo = {}
    userInfo[:followers] = followers["user"]["stats"]["followers"]
    userInfo[:by_day] = Time.now.getlocal('+03:00')
    userInfo[:likes] = followers["user"]["stats"]["appreciations"]
    userInfo[:project_views] = followers["user"]["stats"]["views"]
    # userInfo[:profile_views] = stats["stats"]["all_time"]["profile_views"]

    userInfo
  end

  def setUserInfo
    @statsDB.insert(generateUserInfo())
  end

  def getData (url)
    response = RestClient.get(url)
    JSON.parse(response)
  end

end