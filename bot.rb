# https://www.behance.net/search?content=projects&sort=views&time=week - most popular
# https://www.behance.net/search?content=projects&sort=published_date&time=week  - most freshness

require 'capybara'
require 'pg'
require 'byebug'
require 'capybara/poltergeist'

require_relative './database.rb'
require_relative './comments.rb'

#################################################################

Capybara.javascript_driver = :poltergeist

options = { }
options[:js_errors] = false
options[:default_wait_time] = 30
options[:timeout] = 90
options[:phantomjs_options] = ['--load-images=no']

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, options)
end

class Bot

  def initialize
    @comments = Comments.new
    accounts = DB[:accounts].order(Sequel.asc(:counter)).where(:active, true).all

    @accounts_dribbble = accounts.select {|account|  account[:type] == 'dribbble'}
    @accounts_behance = accounts.select {|account|  account[:type] == 'behance'}

    @session = if ARGV[0] == 'selenium'
      Capybara::Session.new(:selenium)
    else
      Capybara::Session.new(:poltergeist)
    end
  end

  def visit_and_login_behance
    @session.visit "https://adobeid-na1.services.adobe.com/renga-idprovider/pages/login?callback=https%3A%2F%2Fims-na1.adobelogin.com%2Fims%2Fadobeid%2FBehanceWebSusi1%2FAdobeID%2Ftoken%3Fredirect_uri%3Dhttps%253A%252F%252Fwww.behance.net%252F%253Fisa0%253D1%2523from_ims%253Dtrue%2526old_hash%253D%2526api%253Dauthorize%26state%3D%257B%2522ac%2522%253A%2522behance.net%2522%257D&client_id=BehanceWebSusi1&scope=AdobeID%2Copenid%2Cgnav%2Csao.cce_private%2Ccreative_cloud%2Ccreative_sdk%2Cbe.pro2.external_client%2Cadditional_info.roles&denied_callback=https%3A%2F%2Fims-na1.adobelogin.com%2Fims%2Fdenied%2FBehanceWebSusi1%3Fredirect_uri%3Dhttps%253A%252F%252Fwww.behance.net%252F%253Fisa0%253D1%2523from_ims%253Dtrue%2526old_hash%253D%2526api%253Dauthorize%26response_type%3Dtoken%26state%3D%257B%2522ac%2522%253A%2522behance.net%2522%257D&display=web_v2&state=%7B%22ac%22%3A%22behance.net%22%7D&relay=55f28bd5-5bb5-4b83-9fe8-55fa459347eb&locale=ru_RU&flow_type=token&client_redirect=https%3A%2F%2Fims-na1.adobelogin.com%2Fims%2Fredirect%2FBehanceWebSusi1%3Fclient_redirect%3Dhttps%253A%252F%252Fwww.behance.net%252F%253Fisa0%253D1%2523from_ims%253Dtrue%2526old_hash%253D%2526api%253Dauthorize%26state%3D%257B%2522ac%2522%253A%2522behance.net%2522%257D&idp_flow_type=login"

    account = @accounts_behance.sample

    @session.fill_in('username', with: account[:email])
    @session.fill_in('password', with: account[:password])
    @session.click_button('sign_in')
    @session.has_selector?('.behance-logo')

    return account
  end

  def visit_and_login_dribbble
    @session.visit "https://dribbble.com/session/new"

    account = @accounts_dribble.sample

    @session.fill_in('login', with: account[:email])
    @session.fill_in('password', with: account[:password])
    @session.click_button('Sign In')
    @session.has_selector?('#t-profile')

    return account
  end

  def visit_behance url
    @session.visit url

    @session.has_selector?('.covers')
    @session.all('.cover-block').each do |block|
      obj = {}
      obj[:behance_id] = block['data-id']
      obj[:behance_link] = block.find_css('a.cover-img-link')[0][:href]
      obj[:behance_text] = block.find_css('a.cover-name-link')[0][:text]
      p obj
      yield obj
    end
  end

  def visit_dribbble url
    @session.visit url

    @session.has_selector?('.dribbbles group')
    @session.all('li.group').each do |block|
      obj = {}
      obj[:behance_id] = block['data-id']
      obj[:behance_link] = block.find_css('a.cover-img-link')[0][:href]
      obj[:behance_text] = block.find_css('a.cover-name-link')[0][:text]
      p obj
      yield obj
    end
  end

  def visit_behance_link item
      url = item[:behance_link]
      @session.visit url
      sleep(5)
      @session.has_selector?('.project-header')
      @session.has_selector?('[name="comments_form"]')
      @session.execute_script('window.scrollTo(0,100000)')

    begin
      #item[:was_published] = @session.first('[data-timestamp]') ? @session.first('[data-timestamp]')['data-timestamp'] : nil

      p @session.fill_in('comment', with: @comments.get_one, id: 'comment' );
      button = @session.first('.form-button.form-button-default.js-submit')

      button.trigger('click')
      sleep(5)
      yield item
    rescue
      item[:error_message] = "#{$!}"
      yield item      
    end

  end

end