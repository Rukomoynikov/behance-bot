# encoding: utf-8

if ENV['ENV_MODE'] == 'production'
  DB = Sequel.postgres('behance_bot', :user => 'postgres', :host => '127.0.0.1', :password => '725262')
else
  DB = Sequel.postgres('d1mmn3uusfusi7', :user => 'cskuxhypwcylwh', :password => 'p5hMggAv-a71b9bHnPCR8hEyRq', :host => 'ec2-54-75-233-146.eu-west-1.compute.amazonaws.com')
end

##
# Backup Generated: db_backup
# Once configured, you can run the backup with the following command:
#
# $ backup perform -t db_backup [-c <path_to_configuration_file>]
#
Backup::Model.new(:db_backup, 'Description for db_backup') do
  ##
  # Split [Splitter]
  #
  # Split the backup file in to chunks of 250 megabytes
  # if the backup file size exceeds 250 megabytes
  #
  split_into_chunks_of 250

  ##
  # PostgreSQL [Database]
  #
  database PostgreSQL do |db|
    db.name               = "behance_bot"
    db.username           = "postgres"
    db.host               = "127.0.0.1"
    db.port               = 5432
  end

  ##
  # SCP (Secure Copy) [Storage]
  #
  store_with SCP do |server|
    server.username   = "my_username"
    server.password   = "my_password"
    server.ip         = "123.45.678.90"
    server.port       = 22
    server.path       = "~/backups/"
    server.keep       = 5
  end

  ##
  # Gzip [Compressor]
  #
  compress_with Gzip

  ##
  # Mail [Notifier]
  #
  # The default delivery method for Mail Notifiers is 'SMTP'.
  # See the Wiki for other delivery options.
  # https://github.com/meskyanichi/backup/wiki/Notifiers
  #
  notify_by Mail do |mail|
    mail.on_success           = true
    mail.on_warning           = true
    mail.on_failure           = true

    mail.from                 = "sender@email.com"
    mail.to                   = "receiver@email.com"
    mail.address              = "smtp.gmail.com"
    mail.port                 = 587
    mail.domain               = "your.host.name"
    mail.user_name            = "sender@email.com"
    mail.password             = "my_password"
    mail.authentication       = "plain"
    mail.encryption           = :starttls
  end

end
