if ( $('#app').length ){
  new Vue({
    el: "#app",
    data:{
      users: [],
      usersFilterQuery: '',
      usersFiltered: [],
      userEmail: "",
      userPassword: ""
    },
    mounted : function() {
      this.fetchUsers();
    },
    methods: {
      fetchUsers: function() {
        this.$http.get('/api/v1/users').then(function(v1Users) {
          accounts = v1Users.data.accounts
          debugger
          this.$data.users = accounts
          this.$data.usersFiltered = accounts.map(function(account) {
            return account.active == true
          })
        });
      },
      startFilter: function (item) {

      },
      deactivateUser: function (userID, event) {
        this.$http.delete('/api/v1/users', {params: {userID: userID}}).then(function(data){
          console.log(data)
        })
      },
      createUser: function (event) {
        event.preventDefault();

        email = this.$data.userEmail;
        password = this.$data.userPassword

        this.$http.post('/api/v1/users', { email: email, password: password }).then(function(resp){
          if (resp.status == 200) {
            account = resp.data
            this.$data.users.push({
              id: account.id,
              email: email,
              type: 'behance'
            })
            this.$data.userEmail = ''
            this.$data.userPassword = ''
          }
        })
      }
      // deactivateAllUser: function () {
      //   this.$http.post('/api/reset_waitlist_v2', item, function (data){
      //     this.email = data.email;
      //   })
      // }

    }
  })
}

if ( $('#comments-app').length ){
  new Vue({
    el: "#comments-app",
    data:{
      comments: [],
    },
    mounted : function() {
      this.fetchComments()
    },
    methods: {
      fetchComments: function () {

      }
    }
  })
}
