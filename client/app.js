$(document).ready(function() {

  $.ajax({

   url: "cards",

   success:function(data){
    $container = $('table#myTable tbody')
    data = JSON.parse(data);

    generateList(data);
    generateDashboard(data);
    generateStats(data);
    generateTodayStats(data)

    $('.sk-folding-cube').fadeOut('slow', function(){
      $('.allContent').css({
        display: "block",
        opacity: 0
      })
      $('.allContent').css({
        transition: "opacity .3s",
        opacity: 1
      })
    })

    $('body').on('click', '.accountStats button', function() {
      $('.accountStats').toggleClass('accountStats--visible')
    })

    $('body').on('submit', '.newLoginForm', function(event) {
      // event.preventDefault();

    })


   }

  })

});

function generateList (data) {
  items = data.items.map(function(item){
    item.account = _.findWhere(data.accounts, {id: item.account_id}) || {}
    return item
  })

  items.forEach(function(item, index){
    // $container.append("<div>" + index + " " + item.behance_text + " - " +  item.completed +  "</div>")
    was_commented = ""
    if (item.was_commented && item.error_message) {
      was_commented = item.error_message;
    } else if (item.was_commented) {
      was_commented = item.was_commented;
    } else {
      was_commented = "Нет"
    }

    $container.append(`<tr>
      <td>${index}</td>
      <td><a href='${item.behance_link}'>${item.behance_text}</a></td>
      <td>${moment(item.was_parsed).format("D-M-Y HH:m")}</td>
      <td>${item.was_commented == null ? 'Нет' : moment(item.was_commented).format("D-M-Y HH:m")}</td>
      <td>${item.account.email || ""}</td>
    </tr>`)
  })

  $("table#myTable").stupidtable();
}

function generateDashboard (data) {
  $accountsStatusContainer = $('#account-status tbody');
  $botStatusContainer = $('#bot-status');

  accounts = data.by_accounts.map(function(item) {
    item.details = _.findWhere(data.accounts, {id: item.account_id}) || {}
    return item
  })
  accounts = accounts.filter(function(account){
    return account.details.active == true
  })
  accountsHTML = "";
  accounts.forEach(function(account) {
    accountsHTML += `<tr><td>${account.details.email}</td><td>${account.count}</td><td>${account.details.counter}</td></tr>`
  })

  $accountsStatusContainer.html(accountsHTML);
  $botStatusContainer.html(`<h4>Всего ${data.count}, с комментарием: ${data.count_completed}</h4>`)

}

function generateStats(data) {
  $statsHeadContainer = $('table#statsTable thead');
  $statsContainer = $('table#statsTable tbody');
  stats = data.stats

  statsWithRelativeValues = stats.map(function(item, i){
    if (i == 0) {
      // return 0
    } else {
      return (item.followers - stats[i-1].followers)
    }
  })
  // .filter(function(item) {
  //   return item != 0
  // })

  var statsColorGenerator = generatorStatsColor(statsWithRelativeValues, ["grey", "green"])

  var dates = stats.map(function(stat){return moment(stat.by_day).subtract(1, "days").format("D MMM")})
  dates.shift()

  generateChart(dates, statsWithRelativeValues)

  // dateHTML = `<tr><th></th> ${stats.map(function(stat){return "<th>" + moment(stat.by_day).subtract(1, "days").format("D MMM")  + "</th>"})} </tr>`
  // followersHTML = `<tr><td>Followers: </td> ${stats.map(function(stat, i){
  //   return "<td>" + generateNumColor(stat, stats, i, statsColorGenerator) + "</td>"
  // })} </tr>`
  // $statsHeadContainer.html(dateHTML);
  // $statsContainer.html(followersHTML);
}

function generateNumColor (stat, stats, i, color) {
  var num = stat.followers - (stats[i - 1] == undefined ? 0 : stats[i - 1].followers)
  if (i == 0) {
    return `<div style="text-align: center;padding: 5px;">${num}</div>`
  } else {
    return `<div style="text-align: center;padding: 5px;color: white;background-color:${color(num)}">${num}</div>`
  }
}

function generatorStatsColor (arr, colors) {
  var min_max = d3.extent(arr)
  var color = d3.scaleLinear()
      .domain(min_max)
      .range([colors[0], colors[1]]);

  return function (num) {
    return color(num)
  }

}

function generateTodayStats (data) {
  var $statsContainer = $('#todayStats');
  var todayStats = data.todayStats;

  var statsContainerHTML = "<ul>";
  statsContainerHTML += `<li><b>FOLLOWERS</b>: ${todayStats.followers}</li>`;
  statsContainerHTML += `<li><b>APPRECIATIONS</b>: ${todayStats.likes}</li>`;
  statsContainerHTML += `<li><b>PROJECT VIEWS</b>: ${todayStats.project_views}</li>`;
  statsContainerHTML += "</ul>";

  $statsContainer.html(statsContainerHTML);
}

function generateChart (dates, followers) {
  var ctx = document.getElementById("chart");
  var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: dates,
        datasets: [
            {
                type: 'line',
                label: 'Followers',
                data: followers,
                lineTension: 0.1,
                backgroundColor: "rgba(75,192,192,0.4)",
                borderColor: "rgba(75,192,192,1)",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "rgba(75,192,192,1)",
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                spanGaps: false
            }
        ]
      }
  });
}
