# encoding: utf-8
Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = Encoding::UTF_8

require 'sinatra'
require 'json'
require_relative './database.rb'
require 'sinatra/cross_origin'
require 'byebug'
require_relative './bot-api'

configure do
  enable :cross_origin
end

###### Sinatra Part ######

set :port, 8080
set :environment, :production
set :public_folder, File.dirname(__FILE__) + '/client'

use Rack::Auth::Basic do |username, password|
  username == 'mama' && password == 'takethisbadgeoffofme'
end

bot_api = BotAPI.new

get '/' do
  html :index
end

get '/users' do
  html :users
end

get '/comments' do
  html :comments
end


get '/cards' do
  cross_origin

  search = params['search']
  page = params['page']

  items = DB[:behance_links].filter(:completed => true).order(Sequel.desc(:behance_links__was_commented, :nulls=>:last)).limit(20)
  by_accounts = DB[:behance_links].join(:accounts, :id => :account_id).group_and_count(:behance_links__account_id)
  accounts = DB[:accounts]
  stats = DB[:behance_stats].select().where( 'by_day > ?', Date.today << 1 ).order(Sequel.asc(:behance_stats__by_day))
  todayStats = bot_api.generateUserInfo

  return_message = {}
  return_message[:items] = items.all
  return_message[:by_accounts] = by_accounts.all
  return_message[:accounts] = accounts.all
  return_message[:count] = DB[:behance_links].count
  return_message[:stats] = stats.all
  return_message[:todayStats] = todayStats
  return_message[:count_completed] = DB[:behance_links].filter(:completed => true).count

  return_message.to_json
end

# API USERS
get '/api/v1/users' do
  cross_origin

  account_status = params['status']
  accounts = DB[:accounts]

  return_message = {}
  return_message[:accounts] = accounts.all

  return_message.to_json
end

delete '/api/v1/users' do
  userID = params[:userID]
  accounts = DB[:accounts]
  accounts.where(:id => userID).update(:active => false)

  return_message = {}
  return_message[:success] = true
  return_message.to_json
end

post '/api/v1/users' do
  accounts = DB[:accounts]
  json = JSON.parse(request.body.read)

  email = json['email']
  password = json['password']

  newAccount = accounts.insert(:email => email, :password => password, type: 'behance', :active => true)

  return_message = {}
  return_message[:account] = newAccount
  return_message.to_json
end

# API COMMENTS
get '/api/v1/comments' do
  cross_origin

  comments = DB[:comments]

  return_message = {}
  return_message[:comments] = comments.all

  return_message.to_json
end


# put '/account/:id' do
#   params[:loginEmail]
#   params[:loginPassword]
# end

# post '/comments' do
#   params[:loginEmail]
#   params[:loginPassword]
# end

# put '/comment/:id' do
#   params[:loginEmail]
#   params[:loginPassword]
# end

def html(view)
  File.read(File.join('client', "#{view.to_s}.html"))
end
