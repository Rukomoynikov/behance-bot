require './database.rb'

DB.drop_table :behance_links
DB.drop_table :accounts

DB.create_table! :behance_stats do
  primary_key :id
  DateTime :by_day
  Integer :followers
  Integer :likes
  Integer :project_views
  Integer :profile_views
end

DB.create_table! :accounts do
Integer :counter
  primary_key :id
  String :email, :unique => true
  String :password
  String :type
end

DB.create_table! :behance_links do
  primary_key :id
  String :behance_id, :unique => true
  String :behance_link
  String :behance_text
  TrueClass :completed
  foreign_key :account_id, :accounts
  DateTime :was_parsed
  DateTime :was_published
  DateTime :was_commented
end

DB.create_table! :dribble_links do
  primary_key :id
  String :dribbble_id, :unique => true
  String :link
  String :text
  TrueClass :completed
  foreign_key :account_id, :accounts
  DateTime :was_parsed
  DateTime :was_published
  DateTime :was_commented
end

accounts = DB[:accounts]

# Behance accounts
accounts.insert(:email => "goriu.evgenia@yandex.ru", :password => "1980Evgevg", type: 'behance') 
accounts.insert(:email => "irius.bblack@yandex.ru", :password => "1980Iraira", type: 'behance') 
accounts.insert(:email => "tatyat.orlova@yandex.ru", :password => "1980Tattat", type: 'behance')
accounts.insert(:email => "helen.ferby@yandex.ru", :password => "Lovewar1111", type: 'behance')
accounts.insert(:email => "roghers@yandex.ru", :password => "Lollollol12344", type: 'behance')
accounts.insert(:email => "ek.forest@yandex.ru", :password => "Gorgeous1234", type: 'behance')
accounts.insert(:email => "vartova.anna@yandex.ru", :password => "gegegeRe1", type: 'behance')
accounts.insert(:email => "alisa.temuran@yandex.ru", :password => "kolititut123Q", type: 'behance')
accounts.insert(:email => "miller.alexsandra@yandex.ru", :password => "Aloha789", type: 'behance')
accounts.insert(:email => "maria.desert@yandex.ru", :password => "Dezrt1234", type: 'behance')
accounts.insert(:email => "ksenia.buravceva@yandex.ru", :password => "bububuBu1", type: 'behance')
accounts.insert(:email => "manevic.polina@yandex.ru", :password => "huhuhu1W", type: 'behance')

# Dribbble accounts
accounts.insert(:email => "cat1@mos-it.com", :password => "725262", type: 'dribbble')